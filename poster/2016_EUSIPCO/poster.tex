%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% baposter Portrait Poster
% LaTeX Template
% Version 1.0 (15/5/13)
%
% Created by:
% Brian Amberg (baposter@brian-amberg.de)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0paper,portrait,fontscale=0.32]{baposter}
\usepackage[scaled]{helvet}
\usepackage{url}
\renewcommand\familydefault{\sfdefault} 

\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{relsize} % Used for making text smaller in some places
\usepackage{enumitem} % list item spacing
\usepackage{lipsum}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{array}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{tkz-kiviat}
\usetikzlibrary{arrows}

%\graphicspath{{pics/}} % Directory in which figures are stored

\setlist{leftmargin=*,noitemsep}

\definecolor{bordercol}{RGB}{40,40,40} % Border color of content boxes
\definecolor{headercol1}{RGB}{186,215,230} % Background color for the header in the content boxes (left side)
\definecolor{headercol2}{RGB}{80,80,80} % Background color for the header in the content boxes (right side)
\definecolor{headerfontcol}{RGB}{0,0,0} % Text color for the header text in the content boxes
%\definecolor{boxcolor}{RGB}{186,215,230} % Background color for the content in the content boxes
\definecolor{boxcolor}{RGB}{255,255,255} % Background color for the content in the content boxes

\definecolor{RY1}{RGB}{226,0,38}
\definecolor{RY2}{RGB}{254,205,27}
\definecolor{Bl1}{RGB}{38,109,131}
\definecolor{Bl2}{RGB}{136,204,207}
\definecolor{OY1}{RGB}{236,117,40}
\definecolor{OY2}{RGB}{254,207,83}
\definecolor{DB1}{RGB}{28,70,114}
\definecolor{DB2}{RGB}{0,147,196}
\definecolor{Vi1}{RGB}{114,43,74}
\definecolor{Vi2}{RGB}{198,132,116}
\definecolor{PG1}{RGB}{133,146,66}
\definecolor{PG2}{RGB}{199,213,79}
\definecolor{Gr1}{RGB}{91,94,111}
\definecolor{Gr2}{RGB}{162,181,198}
\definecolor{Gn1}{RGB}{44,152,46}
\definecolor{Gn2}{RGB}{157,193,7}

\newcommand{\R}{\textsuperscript{\textregistered}}
\newcommand{\TM}{\textsuperscript{\texttrademark}}

\begin{document}

\background{ % Set the background to an image (background.pdf)
%\begin{tikzpicture}[remember picture,overlay]
%\draw (current page.north west)+(-2em,2em) node[anchor=north west]
%{\includegraphics[height=1.1\textheight]{figures/background}};
%\end{tikzpicture}
}

\begin{poster}{
grid=false,
%borderColor=bordercol, % Border color of content boxes
%headerColorOne=headercol1, % Background color for the header in the content boxes (left side)
%headerColorTwo=headercol2, % Background color for the header in the content boxes (right side)
%headerFontColor=headerfontcol, % Text color for the header text in the content boxes
%boxColorOne=boxcolor, % Background color for the content in the content boxes
bgColorOne=DB2!60,
bgColorTwo=Gn2!60,
borderColor=Vi1!85,
headerColorOne=Bl2!50,
headerColorTwo=Bl1!50,
headerFontColor=DB1,
boxColorOne=Vi2!05,
headershape=roundedright, % Specify the rounded corner in the content box headers
headerfont=\large\sf\bf, % Font modifiers for the text in the content box headers
textborder=none,
background=shadetb,
headerborder=none,
boxshade=plain
}
{}
%
%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR NAME
%----------------------------------------------------------------------------------------
%
{\sf\bf\huge Energy Consumption Analysis of Software\\Polar Decoders on Low Power Processors} % Poster title
{\vspace{1em} Adrien Cassagne, Olivier Aumage, Camille Leroux,\\Denis Barthou and Bertrand Le Gal % Author names
} % Author email addresses
{\includegraphics[trim=2cm 1cm 2cm 1cm,clip=true,scale=0.15]{pics/logo}} % University/lab logo

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------
\headerbox{Exploring Soft ECC Decoding}{name=introduction,column=0,row=0,headerColorOne=RY2!50,headerColorTwo=RY1!50,headerFontColor=RY1}{
  \textbf{Growing interest for Software Defined Radio (SDR)}
  
  \smallskip

  %------------------------------------------------
  \begin{center}
  \includegraphics[width=0.95\linewidth]{figs/communication_chain}
  \captionof{figure}{Simplified communication chain}
  \end{center}
  %------------------------------------------------

  \begin{itemize}
    \item Leverage powerful, energy efficient procs (x86, ARM)
    \item Reduce dev. cost and time to market
    \item Validate and optimize new algorithms
    \item Enable Cloud computing-based architecture for Radio Access Networks 
      (C-RAN)
  \end{itemize}

  Recent \textbf{Successive Cancellation} soft decoder for Polar codes 
  \cite{arikan2009} strongly benefit from modern CPUs capabilities and SIMD 
  units, open the way to a wide optimization range.

  \medskip

  \textbf{Introducing AFF3CT}, a software environment for exploring ECC 
  decoders.
}

%----------------------------------------------------------------------------------------
%	MATERIALS AND METHODS
%----------------------------------------------------------------------------------------
\headerbox{Decoding of Polar Codes}{name=methods,column=0,below=introduction}{
  The \textbf{Successive Cancellation (SC)} decoding algorithm: a depth-first 
  binary tree traversal algorithm based on 3 key functions:
  \begin{eqnarray*}
  \left\{\begin{array}{l c l c l}
  f(\lambda_a,\lambda_b) &=& sign(\lambda_a.\lambda_b).\min(|\lambda_a|,|\lambda_b|)\\
  g(\lambda_a,\lambda_b,s)&=&(1-2s)\lambda_a+\lambda_b\\
  h(s_{a}, s_{b}) &=& (s_{a} \oplus s_{b}, s_{b}).
  \end{array}\right.
  \label{eq:f_g}
  \end{eqnarray*}

  %\medskip

  %------------------------------------------------
  \begin{center}
  \includegraphics[width=0.95\linewidth]{figs/tree_representation_SC_decoder}
  \captionof{figure}{Full SC decoding tree ($N = 16$)}
  \end{center}
  %------------------------------------------------
}

%----------------------------------------------------------------------------------------
%	CONTRIBUTIONS
%----------------------------------------------------------------------------------------
\headerbox{Contribution of this Work}{name=contrib,span=2,column=1,row=0}{ % To reduce this block to 1 column width, remove 'span=2'
  \textbf{Fast and efficient implementations of the SC decoding algorithm on low 
    power ARM processors}
  \begin{itemize}
    \item Based on the Single Instruction Multiple Data (SIMD) CPU capability
    \begin{itemize}
      \item \textbf{Intra-frame} \cite{giard2014, cassagne2015} (SIMD is used to 
        compute many LLRs in a single frame): \textbf{low latency}, 
        \textbf{moderate throughput}
      \item \textbf{Inter-frame} \cite{le-gal2015, cassagne2015} (SIMD is used 
        to process multiple frames in parallel): \textbf{high latency}, 
        \textbf{high throughput}
    \end{itemize} 
    \item Specific SC algorithm optimizations: tree pruning or Fast-SSC 
    \cite{giard2014} (rate 0, rate 1, single parity check and rep.)
  \end{itemize}

  \medskip

  Two different approaches are available:
  \begin{itemize}
    \item \textbf{Generated} \cite{sarkis2014_2, cassagne2015} (gen.): all the 
      recursive calls are unrolled, specific decoder for a given SNR, faster 
      decoders 
    \item \textbf{Dynamic} \cite{giard2014, le-gal2015} (dyn.): the recursive 
    calls are not unrolled, the decoder can adapt dynamically to various SNR
  \end{itemize}

  \textbf{The first work to combine/compare all of these optimizations with 
  energy considerations in mind}
}

%----------------------------------------------------------------------------------------
%	AFF3CT
%----------------------------------------------------------------------------------------
\headerbox{A Fast Forward Error Correction Tool (AFF3CT): Generic ECC Simulation Framework}{name=aff3ct,span=2,column=1,below=contrib}{ % To reduce this block to 1 column width, remove 'span=2'

  \begin{multicols}{2}
    \textbf{\textcolor{red}{AFF3CT}: a software dedicated to simulations of \\
      digital communications with channel coding}

    \begin{center}
      {\color{DB2}\url{http://aff3ct.github.io}}
    \end{center}    

    \begin{itemize}
      % \item {\color{DB2}\url{http://aff3ct.github.io}}
      \item Support many codes: \textbf{Polar}, \textbf{Turbo}, 
        \textbf{Convolutional}, \textbf{Repeat and Accumulate} and \textbf{LDPC} 
        (coming soon)
      \medskip
      \item Very fast simulations, take advantage of today CPUs architecture 
        (\textbf{hundreds of Mb/s on Intel Core i5/7})
      \begin{itemize}
        \item Written in \texttt{C++11} (\textbf{SystemC/TLM support})
        \item Monte-Carlo \textbf{multi-threaded} simulations
        % \item Intensive use of the \textbf{SIMD} (SSE, AVX and NEON)
        % \item Minimal memory allocations
        \item From \textbf{10 to 1000 faster than MATLAB} code
      \end{itemize}
      \smallskip
      \item \textbf{Portable}: run on Linux, Mac OS X and Windows
      \medskip
      \item \textbf{Open-source code} (under MIT license)
      % \begin{itemize}
      %   \item {\color{DB2}\url{http://aff3ct.github.io}}
      % \end{itemize}
    \end{itemize}

    %------------------------------------------------
    \begin{center}
      \includegraphics[width=0.95\linewidth]{graphs/ber_fer}
      % \captionof{figure}{Bit Error Rate (squares) and Frame Error Rate (circles) for the Fast-
      %   SSC decoder. The solid shapes represent the dynamic decoder with adaptive
      %   frozen bits when the hollow shapes represent the generated decoders. For
      %   $N = 4096$, the generated decoder is optimized for 3.2dB. For $N = 32768$,
      %   the generated decoder is optimized for 4.0dB.}
      \captionof{figure}{Simulated BER and FER for the Fast-SSC decoder}
    \end{center}
    %------------------------------------------------
  \end{multicols}
}

%----------------------------------------------------------------------------------------
%	EXPERIMENTS
%----------------------------------------------------------------------------------------
\headerbox{Experiments and Measurements}{name=results1,span=2,column=1,below=aff3ct}{ % To reduce this block to 1 column width, remove 'span=2'
  % The objective and originality of this study is to explore different software 
  % and hardware parameters for the execution of a software SC decoder on modern 
  % ARM architectures. For a software decoder such as AFF3CT, many parameters can
  % be explored, influencing performance and energy efficiency. The target rate 
  % and frame size are applicative parameters. The SIMDization strategies 
  % (intra-frame or inter-frame) and the features of decoders (generated or 
  % dynamic) are software parameters. The target architecture, its frequency and 
  % its voltage are hardware parameters.

  \begin{multicols}{2}

    %------------------------------------------------
    {\footnotesize
      \begin{tabular}{c | c | c | c | c | c}
        \textbf{Cluster} & 
        \textbf{Impl.} &  
        $\boldsymbol{T_i}$ \textbf{(Mb/s)} & 
        $\boldsymbol{l}$   \textbf{($\boldsymbol{\mu}$s)} &
        $\boldsymbol{E_b}$ \textbf{(nJ)} & 
        $\boldsymbol{P}$   \textbf{(W)}\\
        \hline  
        \multirow{3}{*}{\textbf{A7-450MHz}}  & seq.  &           3.1  &        655    &        37.8  &         0.117  \\
                                             & intra &          13.0  &        158    &         9.5  &         0.123  \\
                                             & inter &          21.8  &       1506    &         6.0  &         0.131  \\
        \hline  
        \multirow{3}{*}{\textbf{A53-450MHz}} & seq.  &           2.1  &        966    &        29.0  & \textbf{0.062} \\
                                             & intra &          10.1  &        203    &         7.0  &         0.070  \\
                                             & inter &          17.2  &       1902    & \textbf{5.1} &         0.088  \\
        \hline  
        \multirow{3}{*}{\textbf{A15-1.1GHz}} & seq.  &           7.5  &        274    &       122.0  &         0.913  \\
                                             & intra &          35.2  &         58    &        28.2  &         0.991  \\
                                             & inter &          62.8  &        522    &        17.4  &         1.093  \\
        \hline  
        \multirow{3}{*}{\textbf{A57-1.1GHz}} & seq.  &           9.2  &        222    &        78.9  &         0.730  \\
                                             & intra &          39.2  &         52    &        21.1  &         0.826  \\
                                             & inter &          65.1  &        503    &        14.2  &         0.923  \\
        \hline  
        \multirow{3}{*}{\textbf{i7-3.3GHz}}  & seq.  &          36.3  &         56.5  &       235.4  &         8.532  \\
                                             & intra &         221.8  &  \textbf{9.2} &        40.5  &         9.017  \\
                                             & inter & \textbf{632.2} &         51.8  &        15.8  &         9.997  \\
      \end{tabular}
    }
    \captionof{table}{Characteristics for each cluster ($T_i$ is the information 
      throughput), for dyn. decoder. $N = 4096$, rate $R = 1/2$. 
      The RAM consumption is not included in $E_b$ and in $P$.}
    %------------------------------------------------

    %------------------------------------------------
    \begin{center}
    \includegraphics[width=0.95\linewidth]{graphs/A15_1100MHz_R05_Eb_intra_inter_bis}
    \captionof{figure}{Variation of the \emph{energy-per-bit} for different 
      frame sizes  and impl.: intra-/inter-frame, dyn. code on/off, on A15 @ 
      1.1GHz. Fixed rate $R = 1/2$.}
    \end{center}
    %------------------------------------------------

    %------------------------------------------------
    \begin{center}
    \includegraphics[width=0.95\linewidth]{graphs/A7_250MHz_550MHz_A15_800MHz_1100MHz_R05_N4046_intra_inter_modif2}
    \captionof{figure}{Variation of the \emph{energy-per-bit} ($E_b$) depending 
      on the cluster frequency (dynamic code, intra-, inter-frame). A7 
      performance is on the left and A15 on the right. $N = 4096$ and $R = 1/2$.
      Dark colors and light colors stand for CPU cluster and RAM energy 
      consumption, resp.}
    \end{center}
    %------------------------------------------------

    %------------------------------------------------
    \begin{center}
    \begin{tikzpicture}
    \tkzKiviatDiagram[rotate=90,scale=0.45,label distance=0.5cm,label space=6.25cm, font=\footnotesize,
            radial  = 5,
            gap     = 1,  
            lattice = 4]{Larger SNR range,Lower memory footprint,Lower latency,Lower energy per bit,Higher throughput}
    \tkzKiviatLine[ultra thick,color=red,mark=none,fill= red!20, opacity=.3](4,2,1,3,3)
    \tkzKiviatLine[ultra thick,color=blue,fill=blue!20, opacity=.3](4,4,3,1,1) 
    \tkzKiviatLine[dotted, ultra thick,color=blue](2,3,4,2,2)    
    \tkzKiviatLine[dotted, ultra thick,color=red](2,1,2,4,4)    
    \end{tikzpicture}
    \captionof{figure}{\label{fig:spider}Ranking of the different approaches 
      along 5 metrics. In red, inter-frame vectorization performance and in 
      blue, intra-frame performance. Solid color is for the dynamic versions, 
      dotted is for the generated versions. Each version is sorted along each of 
      the 5 axes and the best version for one axe is placed further from the 
      center.}
    \end{center}
    %------------------------------------------------
  \end{multicols}
}

%----------------------------------------------------------------------------------------
%	CONCLUSION
%----------------------------------------------------------------------------------------
\headerbox{Conclusion}{name=conclusion,column=0,below=methods,headerColorOne=RY2!50,headerColorTwo=RY1!50,headerFontColor=RY1}{
  \medskip
  \textbf{State of the art SC optimizations and performances}
  \begin{itemize}
    \item Inter/intra-frame SIMD implementations
    \item Generated and dynamic decoders
  \end{itemize}

  \medskip
  \textbf{Energy consumption analysis}
  \begin{itemize}
    \item Software SC decoder = only \textbf{14 nJ per bit}, \textbf{65 Mbps} 
      (ARM Cortex-A57 @ 1.1GHz, $N=4096$, $R=1/2$)
    \item Performance and energy consumption comparison on 
      \textbf{big.LITTLE ARM32/64} and \textbf{Intel x86} processors
  \end{itemize}

  \smallskip
}

%----------------------------------------------------------------------------------------
% REFERENCES
%----------------------------------------------------------------------------------------
\headerbox{References}{name=references,below=conclusion,above=bottom,column=0}{
  \medskip
  %\smallskip
  \smaller % Reduce the font size in this block
  \renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
  %\nocite{*} % Insert publications even if they are not cited in the poster

  \bibliographystyle{unsrt}
  \bibliography{biblio} % Use sample.bib as the bibliography file
}

%----------------------------------------------------------------------------------------
% CONTACT
%----------------------------------------------------------------------------------------
\headerbox{Contact}{name=contact,column=1,below=results1,above=bottom,headerColorOne=Gr2!50,headerColorTwo=Gr1!50,headerFontColor=Gr1}{
  % \smaller
  \medskip
  \medskip
  \medskip
  Contact e-mail: {\color{DB2}\url{adrien.cassagne@inria.fr}}
  % %\smallskip
  % \begin{center}
  %   \includegraphics[width=0.20\textwidth]{qr}
  % \end{center}
  % %\smallskip
}



%----------------------------------------------------------------------------------------
%	ACKNOWLEDGEMENTS
%----------------------------------------------------------------------------------------
\headerbox{Acknowledgements}{name=acknowledgements,column=2,below=results1,above=bottom, headerColorOne=Gr2!50,headerColorTwo=Gr1!50,headerFontColor=Gr1}{
  \smaller % Reduce the font size in this block
  \smallskip
  This study has been carried out with financial support from the French 
  State, managed by the French National Research Agency (ANR) in the frame 
  of the "Investments for the future" Programme IdEx Bordeaux - CPU 
  (\textbf{ANR-10-IDEX-03-02}).
} 

%----------------------------------------------------------------------------------------

\end{poster}

\end{document}
