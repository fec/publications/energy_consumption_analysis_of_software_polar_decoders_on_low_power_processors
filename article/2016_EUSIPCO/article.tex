%% bare_conf.tex
%% V1.4b
%% 2015/08/26
%% by Michael Shell
%% See:
%% http://www.michaelshell.org/
%% for current contact information.
%%
%% This is a skeleton file demonstrating the use of IEEEtran.cls
%% (requires IEEEtran.cls version 1.8b or later) with an IEEE
%% conference paper.
%%
%% Support sites:
%% http://www.michaelshell.org/tex/ieeetran/
%% http://www.ctan.org/pkg/ieeetran
%% and
%% http://www.ieee.org/

%%*************************************************************************
%% Legal Notice:
%% This code is offered as-is without any warranty either expressed or
%% implied; without even the implied warranty of MERCHANTABILITY or
%% FITNESS FOR A PARTICULAR PURPOSE! 
%% User assumes all risk.
%% In no event shall the IEEE or any contributor to this code be liable for
%% any damages or losses, including, but not limited to, incidental,
%% consequential, or any other damages, resulting from the use or misuse
%% of any information contained here.
%%
%% All comments are the opinions of their respective authors and are not
%% necessarily endorsed by the IEEE.
%%
%% This work is distributed under the LaTeX Project Public License (LPPL)
%% ( http://www.latex-project.org/ ) version 1.3, and may be freely used,
%% distributed and modified. A copy of the LPPL, version 1.3, is included
%% in the base LaTeX documentation of all distributions of LaTeX released
%% 2003/12/01 or later.
%% Retain all contribution notices and credits.
%% ** Modified files should be clearly indicated as such, including  **
%% ** renaming them and changing author support contact information. **
%%*************************************************************************


% *** Authors should verify (and, if needed, correct) their LaTeX system  ***
% *** with the testflow diagnostic prior to trusting their LaTeX platform ***
% *** with production work. The IEEE's font choices and paper sizes can   ***
% *** trigger bugs that do not appear when using other class files.       ***
% The testflow support page is at:
% http://www.michaelshell.org/tex/testflow/

\documentclass[conference]{IEEEtran}
\usepackage{amssymb}
\usepackage{cite}
\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
\else
  \usepackage[dvips]{graphicx}
\fi
\usepackage{algorithmic}
\usepackage{array}
\ifCLASSOPTIONcompsoc
  \usepackage[caption=false,font=normalsize,labelfont=sf,textfont=sf]{subfig}
\else
  \usepackage[caption=false,font=footnotesize]{subfig}
\fi
\usepackage{url}
\usepackage{color}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{amsmath,graphicx}
\usepackage{pgfplots}
\usepackage{xspace}
\usepackage{siunitx}
\usepackage{bm}
\usepackage{tkz-kiviat}
\usetikzlibrary{arrows}

\def\TODO#1{\colorbox{yellow}{\bf TODO: #1}}
\def\bl{big.\textsc{LITTLE}\xspace}
\def\big{big\xspace}
\def\little{\textsc{LITTLE}\xspace}
\def\odr{\textsc{Odroid}\xspace}
\def\odrx{\textsc{Odroid-XU+E}\xspace}
\def\juno{\textsc{Juno}\xspace}
\def\mus{\si{\micro\second}}
\setlength{\textfloatsep}{5pt}
\setlength{\parsep}{0pt}
\setlength{\parskip}{0pt}

\newcommand{\Arikan}{Ar{\i}kan }
\newcommand{\MISSREF}[1]{\textcolor{red}{\textbf{[MISS REF:#1]}}}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
\title{Energy Consumption Analysis of Software \\
Polar Decoders on Low Power Processors}

\author{\IEEEauthorblockN{Adrien Cassagne\IEEEauthorrefmark{1}\IEEEauthorrefmark{2},
Olivier Aumage\IEEEauthorrefmark{2},
Camille Leroux\IEEEauthorrefmark{1},
Denis Barthou\IEEEauthorrefmark{2} and
Bertrand Le Gal\IEEEauthorrefmark{1}
}
\IEEEauthorblockA{\IEEEauthorrefmark{1}IMS Lab, Bordeaux INP, France}
\IEEEauthorblockA{\IEEEauthorrefmark{2}Inria /  Labri, Univ. Bordeaux, INP, France}}

% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract
\begin{abstract}
This paper presents a new dynamic and fully generic implementation of a 
Successive Cancellation (SC) decoder (multi-precision support and 
intra-/inter-frame strategy support). This fully generic SC decoder is used to 
perform comparisons of the different configurations in terms of throughput,
latency and energy consumption. A special emphasis is given on the energy 
consumption on low power embedded processors for software defined radio (SDR) 
systems. A N=4096 code length, rate 1/2 software SC decoder consumes only 14 nJ 
per bit on an ARM Cortex-A57 core, while achieving 65 Mbps. Some design 
guidelines are given in order to adapt the configuration to the application 
context.
\end{abstract}

% no keywords

\IEEEpeerreviewmaketitle

\section{Introduction}
\label{sec:intro}
Channel coding enables transmitting data over unreliable communication channels. 
While error correction coding/decoding is usually performed by dedicated 
hardware circuits on communication devices, the evolution of general 
purpose processors in terms of energy efficiency and parallelism (vector 
processing, number of cores,...) drives a growing interest for software ECC 
implementations (e.g. LDPC decoders~\cite{Wan13a,legal2015a,Gal:2015aa}, 
\emph{Turbo} decoders~\cite{6167744,6810402}). The family of the
\emph{Polar} codes has been introduced recently. They asymptotically reach 
the capacity of various communication channels \cite{arikan2009}. They can be 
decoded using a successive cancellation (SC) decoder, which has extensively been 
implemented in hardware~\cite{mishra_successive_2012,Leroux:2012aa,Raymond:2014aa,Li:2014aa,Yuan:2014aa,sarkis_fast_polar_2014,Giard:2015aa}.
Several software decoders have also been proposed 
\cite{sarkis2014_1,giard2014,sarkis2014_2,le-gal2014,le-gal2015,cassagne2015}, 
all employing Single Instruction Multiple Data 
(SIMD) instructions to reach multi-Gb/s performance. 
Two SIMD strategies deliver high performance: the \emph{intra}-frame
parallelism strategy~\cite{sarkis2014_1,giard2014,sarkis2014_2} delivers both high
throughput and low latency; the \emph{inter}-frame parallelism
strategy~\cite{le-gal2014, le-gal2015} improves the throughput
performance by a better use of the SIMD unit width at the
expense of a higher latency. AFF3CT\footnote{AFF3CT is an Open-source software 
(MIT license) for fast forward error correction simulations, see 
\texttt{http://aff3ct.github.io}} 
\cite{cassagne2015, aff3ct_2016_55668} (previously called P-EDGE) is the first 
software SC decoder to include both parallelism strategies as well as 
state-of-the-art throughput and latency. 

The optimization space exploration for SC decoding of Polar codes has
so far primarily been conducted with raw performance in mind. However,
the energy consumption minimization should also be factored in.
Moreover, heterogeneous multi-core processors such as ARM's \bl
architectures offer cores with widely different performance and energy
consumption profiles, further increasing the number of design and
run-time options.
In this context, the contribution of this paper is to propose a new dynamic SC 
decoder, integrated into our AFF3CT software and to derive key guidelines and 
general strategies in balancing performance and energy consumption 
characteristics of software SC decoders.

The remainder of this paper is organized as follows. Section~\ref{sec:polar} 
details relevant characteristics of the general Polar code encoding/decoding 
process. Section \ref{sec:state} discusses related works in the 
domain. Section \ref{sec:dyngen} describes our proposed dynamic SC decoder and 
compares it to our previous \emph{specialized} approach based on code
generation. Section \ref{sec:explore} presents various characteristics
to explore in order to reach a performance trade-off. Section
\ref{sec:measurements} presents experiments and comments on
performance results.

\section{Polar codes encoding and decoding}

\begin{figure}[t]
\centering
\includegraphics[width=1.0\linewidth]{./diagrams/tree_representation_SC_decoder}
\caption{Full SC decoding tree ($N = 16$)}
\label{fig:dec_tree}
\end{figure}

\label{sec:polar}

%\subsection{Systematic Encoding}
Polar codes are linear block codes of size $N=2^n$, $n\in\mathbb{N}$.
In \cite{arikan2009}, \Arikan defined their construction based on 
the $n^{th}$ Kronecker power of a kernel matrix 
$\kappa = \left[\begin{array}{cc}1& 0\\ 1& 1\end{array}\right] $, denoted 
${\kappa}^{\otimes n}$.
The systematic encoding process \cite{arikan2011} consists in building an 
$N$-bit vector $V$ including $K$ information bits and $N-K$ frozen bits, 
usually set to zero. The location of the frozen bits depends on both the type of 
channel that is considered and the noise power on the channel 
\cite{arikan2009}. Then, a first encoding phase is performed: 
$U=V*\kappa^{\otimes n}$ and bits of $U$ in the frozen location are replaced by 
zeros. The codeword is finally obtained with a second encoding phase: 
$X=U*\kappa^{\otimes n}$. In this systematic form $X$ includes $K$ information 
bits and $N-K$ redundancy bits located on the frozen locations.

%\subsection{SC Decoding}
After being sent over the transmission channel, the noisy version of the 
codeword $X$ is received as a log likelihood ratio (LLR) vector $Y$. 
The SC decoder successively estimates each bit $u_i$ 
based on the vector $Y$ and the previously estimated bits 
($[\hat{u}_0 ... \hat{u}_{i-1}]$). 
To estimate each bit $u_i$, the decoder computes the following LLR 
value:

{\small
\begin{equation*}
\lambda_{i}^{0} = \log\frac{\Pr(Y,\hat{u}_{0:i-1}|u_i=0)}{\Pr(Y,\hat{u}_{0:i-1}|u_i=1)}.
\end{equation*}
}

The estimated bit $\hat{u}_i$ is $0$ if $\lambda_{i}^{0}>0$, $1$ otherwise. 
Since the decoder knows the location of the frozen bits, if $u_i$ is a frozen 
bit, $\hat{u}_i=0$ regardless of $\lambda_{i}^{0}$ value.
The SC decoding process can be seen as the traversal of a binary tree as shown 
in Figure~\ref{fig:dec_tree}. The tree includes $\log N + 1$ layers each 
including $2^d$ nodes, where $d$ is the depth of the layer in the tree. Each 
node contains a set of $2^{n-d}$ LLRs and partial sums $\hat{s}$. Nodes are 
visited using a pre-order traversal. As shown in Figure~\ref{fig:dec_tree}, 
three functions, $f$, $g$ and $h$ are used for node updates:

{\small
\begin{eqnarray*}
\left\{\begin{array}{l c l}
f(\lambda_a,\lambda_b) &=& sign(\lambda_a.\lambda_b).\min(|\lambda_a|,|\lambda_b|)\\
g(\lambda_a,\lambda_b,s)&=&(1-2s)\lambda_a+\lambda_b\\
h(s_a,s_b)&=& (s_{a} \oplus s_{b}, s_{b})
\end{array}\right.
\label{eq:f_g}
\end{eqnarray*}
}

 The $f$ function is applied 
when a left child node is accessed: 
$\lambda_{i}^{left}=f(\lambda_{i}^{up},\lambda_{i+2^d}^{up}),0\leq i<2^d$. 
The $g$ function is used when a right child node is accessed: 
$\lambda_{i}^{right}=g(\lambda_i^{up},\lambda_{i+2^d}^{up}),0\leq i<2^d$.
Then moving up in the tree, the first half of partial 
sum is updated with $s_i^{up}=h(s_i^{left}, s_i^{right}),0\leq i<2^d/2$ 
and the second half is simply copied : $s_i^{up} = s_i^{right}$.
The decoding process stops when the partial sum of the root node is updated. In
a systematic Polar encoding scheme, this partial sum is the 
decoded codeword.
In practice, by exploiting knowledge on the frozen bits fixed location, whole sub-trees can
be pruned and replaced by specialized nodes \cite{alamdar-yazdi2011,sarkis2014_1}, replacing scalar
computations in the lowest levels of the tree by vector ones.

\section{Software SC Decoders State-of-The-Art}
\label{sec:state}

In \cite{sarkis2014_1,giard2014,sarkis2014_2}, SIMD units  process several 
LLRs in parallel \emph{within} a single frame decoding. This approach, called 
\emph{intra-frame} vectorization is efficient in the upper layers of the tree 
and in the specialized nodes, but more limited in the lowest layers where the 
computation becomes more sequential.

In \cite{le-gal2014,le-gal2015}, an alternative scheme called 
\emph{inter-frame} vectorization decodes several independent frames in parallel 
in order to saturate the SIMD unit. This approach improves the throughput of the 
SC decoder but requires to load several frames before starting to decode, 
increasing both the decoding latency and  the decoder memory footprint.

The AFF3CT software for SC decoding \cite{cassagne2015} is a multi-platform tool 
(x86-SSE, x86-AVX, ARM32-NEON, ARM64-NEON) including all state-of-the-art 
advances in software SC decoding of Polar codes: intra/inter-frame 
vectorization, multiple data formats (8-bit fixed-point, 32-bit floating-point) 
and all known tree pruning strategies. It resorts to code generation strategies 
to build specialized decoders, trading flexibility (code rate $R$, code length 
$N$) for extra performance.

All state of the art implementations aim at providing different trade-offs 
between error correction performance throughput and decoding latency. However, 
energy consumption is also a crucial parameter in SDR systems, as highlighted in 
\cite{wyglinski2009, dutta2010, shaik2013}. In this study, we propose to 
investigate the influence of several parameters on the energy consumption of SC 
software Polar decoders on embedded processors to demonstrate their 
effectiveness for future SDR systems.

\section{Dynamic versus Generated Approach}
\label{sec:dyngen}

\begin{figure}[t]
\centering
\includegraphics[width=0.49\textwidth]{graphics_ber_fer/ber_fer}
\caption{Bit Error Rate (squares) and Frame Error Rate (circles) for the 
  Fast-SSC decoder. The solid shapes represent the dynamic decoder with adaptive 
  frozen bits when the hollow shapes represent the generated decoders. For $N=4096$
  , the generated decoder is optimized for 3.2dB. For $N=32768$, the generated 
  decoder is optimized for 4.0dB.}
\label{plot:ber_fer}
\end{figure}

We extend the AFF3CT software with a new version of the Fast-SSC decoder, 
called \emph{dynamic} decoder. This version uses the same building blocks as the 
generated versions, but the same code is able to accommodate with different 
frozen bit layouts and different parameters (length, SNR). \texttt{C++11} 
template specialization features are used to enable the compiler to perform loop 
unrolling starting from a selected level in the decoding tree. It is the first 
non-generated version (to the best of our knowledge) to support both 
multi-precision (32-bit, 8-bit) and multi-SIMD strategies (intra-frame or 
inter-frame).

By design, generated decoders are still faster than the dynamic decoder
(up to 20\%). However each generated decoder is optimized for a single
SNR. For very large frame sizes, the
dynamic decoder outperforms generated decoders because the heavily unrolled 
generated decoders exceed Level 1 instruction cache size 
capacity~\cite{cassagne2015}.

Fig.~\ref{plot:ber_fer} shows the Bit Error Rate (BER) and the Frame Error Rate 
(FER) of our dynamic and different generated decoders for $N = 4096$ and for 
$N=32768$. Since there is almost no performance degradation between the 8-bit 
fixed-point decoders and the 32-bit floating-point ones, only 8-bit results are 
shown. We observe that the BER/FER performance is better for the dynamic version 
than for the generated codes. Indeed the generated versions are by definition
optimized for a fixed set of frozen bits, and optimal for 3.2dB for
$N=4096$ and 4.0dB for $N=32768$. As a result the generated versions are only 
competitive for a narrow SNR sweet spot. A decoder for a wider range of SNR 
values requires to combine many different generated versions.

\section{Exploring Performance Trade-off}
\label{sec:explore}

The objective and originality of this study is to explore different software and 
hardware parameters for the execution of a software SC decoder on modern ARM 
architectures. For a software decoder such as AFF3CT, many parameters can be 
explored, influencing performance and energy efficiency. The target rate and 
frame size are applicative parameters. The SIMDization strategies (intra-frame 
or inter-frame) and the features of decoders (generated or dynamic) are software 
parameters. The target architecture, its frequency and its voltage are hardware 
parameters. This study investigates the correlations between these parameters, 
in order to better choose the right implementation for a given applicative 
purpose. The low-power general purpose ARM32 and ARM64 processor test-beds based 
on big.LITTLE architecture are selected as representative of modern multi-core 
and heterogeneous architectures. The SC decoder is AFF3CT~\cite{cassagne2015}, 
enabling the comparison of different vectorization schemes.

The flexibility of the AFF3CT software allows to alter many parameters and turn 
many optimizations on or off, leading to a large amount of potential 
combinations. For the purpose of this study, computations are performed with 
8-bit fixed-point data types, with all tree pruning optimizations activated. The 
main metric considered is the average amount of energy in Joules to decode one 
bit of information, expressed  as  $E_b = (P \times l) / (K \times n_f)$ where 
$P$ is the average power (Watts), $l$ is the latency (s), $K$ the number of 
information bits and $n_f$ is the number of frames decoded in parallel (in the 
inter-frame implementation $n_f > 1$).

\textbf{Testbed.} The experiments are conducted on two ARM~\bl platforms, an 
\odrx board using a 32-bit Samsung Exynos 5410 CPU and the reference 64-bit 
\juno Development Platform from ARM running a Linux operating system, detailed 
in Table~\ref{tab:specs}.

%\begin{table}[ht]
\begin{table}[t]
\caption{Specifications of the \odr and the \juno boards.}
\label{tab:specs}
\begin{center}
{\footnotesize
\begin{tabular}{c | c | c}
                                    & \textbf{ODROID-XU+E} &          \textbf{\juno} \\
  \hline  
  \multirow{2}{*}{\textbf{SoC}}     &  Samsung Exynos 5410 &               ARM64 \bl \\
                                    &      (Exynos 5 Octa) &         (dev. platform) \\
  \hline  
  \multirow{1}{*}{\textbf{Arch.}}   &       32-bit, ARMv7  &           64-bit, ARMv8 \\
  \hline  
  \multirow{1}{*}{\textbf{Process}} &                 28nm &  unspecified (32/28 nm) \\

  \hline  
  \multirow{4}{*}{\textbf{\big}}    &  4xCortex-A15 MPCore &     2xCortex-A57 MPCore \\
                                    &   freq. [0.8-1.6GHz] &     freq. [0.45-1.1GHz] \\
                                    &   L1I 32KB, L1D 32KB &      L1I 48KB, L1D 32KB \\
                                    &               L2 2MB &                  L2 2MB \\
  \hline  
  \multirow{4}{*}{\textbf{\little}} &   4xCortex-A7 MPCore &     4xCortex-A53 MPCore \\
                                    &   freq. [250-600MHz] &      freq. [450-850MHz] \\
                                    &   L1I 32KB, L1D 32KB &      L1I 32KB, L1D 32KB \\
                                    &             L2 512KB &                  L2 1MB \\
\end{tabular}
}
\end{center}
\end{table}

The \big and the \little clusters of cores on the \odr board are on/off in a 
mutually exclusive way. The active cluster is selected through the Linux 
\texttt{cpufreq} mechanism. Both clusters can be activated together or 
separately on the \juno board. Both platforms report details on supply voltage, 
current amperage, power consumption for each cluster. Only the \odr platform 
reports details for the RAM.
Consequently, most experiments have been primarily conducted on the \odr
platform to benefit from the additional insight provided by the RAM
metrics.

\section{Experiments and Measurements}
\label{sec:measurements}


\begin{table}[t]
\caption{Characteristics for each cluster
  ($T_i$ is the information throughput), for dyn. decoder.
  $N = 4096$, rate $R = 1/2$. 
 The RAM consumption is not included in 
  $E_b$ and in $P$.}
\label{tab:res}
\begin{center}
{\footnotesize
\begin{tabular}{c | c | c | c | c | c}
  \textbf{Cluster} & 
  \textbf{Impl.} &  
  $\boldsymbol{T_i}$ \textbf{(Mb/s)} & 
  $\boldsymbol{l}$   \textbf{($\boldsymbol{\mu}$s)} &
  $\boldsymbol{E_b}$ \textbf{(nJ)} & 
  $\boldsymbol{P}$   \textbf{(W)}\\
  \hline  
  \multirow{3}{*}{\textbf{A7-450MHz}}  & seq.  &  3.1 &  655 &  37.8 & 0.117 \\
                                       & intra & 13.0 &  158 &   9.5 & 0.123 \\
                                       & inter & 21.8 & 1506 &   6.0 & 0.131 \\
  \hline  
  \multirow{3}{*}{\textbf{A53-450MHz}} & seq.  &  2.1 &  966 &  29.0 & 0.062 \\
                                       & intra & 10.1 &  203 &   7.0 & 0.070 \\
                                       & inter & 17.2 & 1902 &   5.1 & 0.088 \\
  \hline  
  \multirow{3}{*}{\textbf{A15-1.1GHz}} & seq.  &  7.5 &  274 & 122.0 & 0.913 \\
                                       & intra & 35.2 &   58 &  28.2 & 0.991 \\
                                       & inter & 62.8 &  522 &  17.4 & 1.093 \\
  \hline  
  \multirow{3}{*}{\textbf{A57-1.1GHz}} & seq.  &  9.2 &  222 &  78.9 & 0.730 \\
                                       & intra & 39.2 &   52 &  21.1 & 0.826 \\
                                       & inter & 65.1 &  503 &  14.2 & 0.923 \\
  \hline  
  \multirow{3}{*}{\textbf{i7-3.3GHz}} & seq.  &  36.3 & 56.5 & 235.4 &  8.532 \\
                                      & intra & 221.8 &  9.2 &  40.5 &  9.017 \\
                                      & inter & 632.2 & 51.8 &  15.8 &  9.997 \\
\end{tabular}
}
\end{center}
\end{table}

Table~\ref{tab:res} gives an overview of the decoder behavior on different 
clusters and for various implementations. The code is always single threaded and 
only the 8-bit fixed-point decoders are considered, since 32-bit floating-point 
versions  are 4 times more energy consuming, on average. The sequential version 
is mentioned for reference only, as the  throughput $T_i$ is much higher on 
vectorized versions. Generally the inter-frame SIMD strategy delivers better 
performance at the cost of a higher latency $l$. Table~\ref{tab:res} also 
compares the energy consumption of \little and \big clusters. The A53 consumes 
less energy than the A7 and the A57 consumes less energy than the A15, 
respectively. This can be explained by  architectural improvements brought by 
the more recent ARM64 platform. Despite the fact that the ARM64 is a development 
board, the ARM64 outperforms the ARM32 architecture. Finally we observe that the 
power consumption is higher for the inter-frame version than for the intra-frame 
one because it fills the SIMD units more intensively, and the SIMD units consume 
more than the scalar pipeline. 

For comparison, the results for the Intel Core i7-4850HQ, using SSE4.1 
instructions (same vector length as ARM NEON vectors) are also included. Even if 
the i7 is competitive with the ARM big cores in term of \textit{energy-per-bit} 
($E b$), these results show it is not well suited for the low power SDR systems 
because of its high power requirements.
\begin{table}[htp]
\caption{Comparison of 8-bit fixed-point decoders with intra-frame 
  vectorization. $N = 32768$ and $R = 5/6$.}
\label{tab:perf}
\begin{center}
{\footnotesize
\begin{tabular}{c | c | c | c | c | c}
  \textbf{Decoder} &  
  \textbf{Platform} &  
  \textbf{Freq.} &  
  \textbf{SIMD} &  
  $\boldsymbol{T_i}$ \textbf{(Mb/s)} & 
  $\boldsymbol{l}$   \textbf{($\boldsymbol{\mu}$s)}\\
  \hline  
  \cite{giard2014} & i7-2600   & 3.4Ghz & SSE4.1 &         204  &  135 \\
  \hline
  this work        & i7-4850HQ & 3.3Ghz & SSE4.1 & \textbf{580} &   47 \\
  \hline
  this work        & A15       & 1.1Ghz & NEON   &          70  &  391 \\
  \hline
  this work        & A57       & 1.1Ghz & NEON   &          73  &  374 \\
\end{tabular}
}
\end{center}
\end{table}
Table~\ref{tab:perf} shows a performance comparison (throughput, latency) with
the dynamic intra-frame decoder of \cite{giard2014}.
On a x86 CPU, our dynamic decoder is 2.8 times faster than the state-of-the-art 
decoder. Even if we used a more recent CPU, we also used the same set of 
instructions (SSE4.1) and the frequencies are comparable.

\begin{figure}[b]
\centering
\includegraphics[width=0.49\textwidth]{graphics/outputs/A15_1100MHz_R05_Eb_intra_inter_bis}
\caption{Variation of the \emph{energy-per-bit} for different frame 
  sizes  and impl.: intra-/inter-frame, dyn. code on/off, on A15 @ 1.1GHz. 
  Fixed rate $R = 1/2$.}
\label{plot:frame_size}
\end{figure}

Figure~\ref{plot:frame_size} shows the \emph{energy-per-bit} consumption 
depending on the frame size $N$ for the fixed rate $R = 1/2$. In general, the 
energy consumption increases with the frame size. For small frame sizes ($N$ 
from $2^{8}$ to $2^{14}$), the inter-frame SIMD outperforms the intra-frame 
SIMD. This is especially true for $N = 2^8$ which has a low ratio of SIMD 
computations over scalar computations in the intra-frame version. As the frame 
size increases, the ratio of SIMD vs scalar computations increases as well.
At some point around $N = 2^{16}$ the intra-frame implementation begins 
to outperform the inter-frame one, because the data for the intra-frame decoder 
still fits in the CPU cache, whereas the data of the inter-frame decoder does 
not fit the cache anymore. In our case (8-bit fixed point numbers and 128-bit 
vector registers) the inter-frame decoders require 16~times more memory than the 
intra-frame decoders. Then, for the frame size $N = 2^{20}$, both intra and 
inter-frame decoders now exceed the cache capacity and the RAM power consumption 
becomes more significant due to the increased number of cache misses causing RAM 
transactions. In general the code generation is effective on the intra-frame 
strategy whereas it is negligible on the inter-frame version of the code.

Considering those previous observations, it is more energy efficient to use 
inter-frame strategy for small frame sizes, whereas it is better to apply 
intra-frame strategy for larger frame sizes (comparable energy consumption with 
much lower latency).

Figure~\ref{plot:freq} shows the impact of the frequency on the energy, for a 
given value of frame size $N=4096$ and code rate $R=1/2$. On both A7 and A15
clusters, the supply voltage increases with the frequency from 0.946V to
1.170V. The A7 \little cluster shows that the energy consumed by the system RAM 
is significant: At 250MHz it accounts for half of the energy cost. Indeed, at 
low frequency, the long execution time due to the low throughput causes a high 
dynamic RAM refreshing bill. It is therefore more interesting to use frequencies 
higher than 250MHz. For this problem size and configuration, and from an 
energy-only point of view, the best choice is to run the decoder at 350MHz. On 
the A15 \big cluster, the energy cost is mainly driven by the CPU frequency, 
while the RAM energy bill is limited compared to the CPU. 

\begin{figure}[t]
\centering
\includegraphics[width=0.49\textwidth]{graphics/outputs/A7_250MHz_550MHz_A15_800MHz_1100MHz_R05_N4046_intra_inter_modif2}
\caption{Variation of the \emph{energy-per-bit} ($E_b$) depending on the cluster
  frequency (dynamic code, intra-, inter-frame). 
  A7 performance is on the left and A15 on the right. $N = 4096$ and $R = 1/2$.
  Dark colors and light colors stand for CPU cluster and RAM energy consumption, 
  resp.}
\label{plot:freq}
\end{figure}


Thus, the bottom line about energy vs frequency relationship is: On the \little 
cluster it is more interesting to clock the CPU at high frequency (higher 
throughput and smaller latency for a small additional energy cost); On the 
\big cluster, where the RAM consumption is less significant, it is better to 
clock the CPU at a low frequency.

\begin{figure}[b]
%\begin{figure}[t]
\centering
\includegraphics[width=0.49\textwidth]{graphics/outputs/rate_N32768_SNR25}
\caption{Variation of the \emph{energy-per-bit} ($E_b$) for $N = 32768$ 
  depending on the rate $R = K / N$ (various impl.: intra-, inter-frame, code 
  gen. on). Running on A7, A53 and A57 @ 450MHz.}
\label{plot:rate}
\end{figure}

In Figure~\ref{plot:rate} the \emph{energy-per-bit} cost decreases when the code 
rate increases. This is expected because there are many more information bits in 
the frame when $R$ is high, making the decoder more energy efficient. With high 
rates, the SC decoding tree can be pruned more effectively, making the decoding 
process even more energy efficient. Figure~\ref{plot:rate} also compares the ARM 
A7, A53 and A57 clusters for the same 450MHz frequency (note: this frequency is
not available on the A15). The \little A7 is more energy efficient than the \big 
A57, and the \little A53 is itself more energy efficient than the \little A7 
($E_{b_{A53}} < E_{b_{A7}} < E_{b_{A57}}$).

\begin{figure}[t]
\centering
\begin{tikzpicture}
\tkzKiviatDiagram[rotate=90,scale=0.45,label distance=0.5cm,label space=6cm, font=\footnotesize,
        radial  = 5,
        gap     = 1,  
        lattice = 4]{Larger SNR range,Lower memory footprint,Lower latency,Lower energy per bit,Higher throughput}
\tkzKiviatLine[ultra thick,color=red,mark=none,fill= red!20, opacity=.3](4,2,1,3,3)
\tkzKiviatLine[ultra thick,color=blue,fill=blue!20, opacity=.3](4,4,3,1,1) 
\tkzKiviatLine[dotted, ultra thick,color=blue](2,3,4,2,2)    
\tkzKiviatLine[dotted, ultra thick,color=red](2,1,2,4,4)    
\end{tikzpicture}
\caption{\label{fig:spider}Ranking of the different approaches along 5 metrics. 
  In red, inter-frame vectorization performance and in blue, intra-frame 
  performance. Solid color is for the dynamic versions, dotted is for the 
  generated versions. Each version is sorted along each of the 5 axes and the 
  best version for one axe is placed further from the center.}
\end{figure}

Figure \ref{fig:spider} presents a qualitative summary of the
characteristics of the different code versions, for intra-/inter-frame
vectorization, generated or dynamic code. For instance, if the size of
the memory footprint is an essential criterion, the dynamic intra-frame
code exhibits the best performance.

To sum up, the dynamic implementations provides efficient trade-off between 
throughput, latency and energy depending on code length. It was demonstrated by 
previous benchmarks. Both implementations provide low-energy and low-power 
characteristics compared to previous works in the field on x86 processors 
\cite{sarkis2014_1,giard2014,sarkis2014_2,le-gal2014,le-gal2015, cassagne2015}. 
Whereas the throughput on a single processor core is reduced compared to x86 
implementations, ARM implementations must fulfil a large set of SDR applications 
with limited throughputs and where the power consumption matters. Finally, it is 
important to notice that multi-core implementations of the proposed ARM decoders 
is still possible on these ARM targets to improve the decoding throughputs.

\section{Conclusion and Future Work}
\label{sec:conc}
This paper presented for the first time a study comparing performance and energy
consumption for software Successive Cancellation Polar decoders on big.LITTLE 
ARM32 and ARM64 processors. We proposed a new decoder implementation, and showed 
how decoding performance, throughput and decoder implementation correlate for a 
range of applicative parameters, software optimizations and hardware 
architectures.

\section*{Acknowledgements}
\footnotesize{This study has been carried out with financial support from the 
French State, managed by the French National Research Agency (ANR) in the frame 
of the "Investments for the future" Programme IdEx Bordeaux - CPU 
(ANR-10-IDEX-03-02).}

\bibliographystyle{IEEEtran}
\bibliography{article}

\end{document}
