#!/usr/bin/perl -F, -an

BEGIN{ print "PROC,TYPE,GEN,VEC,N,K,R,SNR (dB),FRA,LD+ST+DEC (ms),CTHR (Mbit/s),ITHR (Mbit/s),LATENCY (us),CLU_FREQ (MHz),CLU_POW (W),CLU_ENE (J),RAM_POW (W),RAM_ENE (J),EB_CLU (nJ),EB_CLU_RAM (nJ),TB_CLU (ns)\n"; }
chomp $F[$#F];

$PROC      = $F[0];
$TYPE      = $F[1];
$GEN       = $F[2];
$VEC       = $F[3];
$N         = $F[4];
$K         = $F[5];
$R         = $F[6];
$SNR       = $F[7];
$FRA       = $F[8];
$LD_ST_DEC = $F[9];
$CTHR      = $F[10];
$ITHR      = $F[11];
$LATENCY   = $F[12];
$BIG_FREQ  = $F[14];
$BIG_POW   = $F[15];
$BIG_ENE   = $F[16];
$LIT_FREQ  = $F[17];
$LIT_POW   = $F[18];
$LIT_ENE   = $F[19];
$RAM_POW   = $F[20];
$RAM_ENE   = $F[21];

if (($PROC eq "A07") || ($PROC eq "A53")) {
	$CLU_FREQ = $LIT_FREQ;
	$CLU_POW  = $LIT_POW;
	$CLU_ENE  = $LIT_ENE;
} else {
	$CLU_FREQ = $BIG_FREQ;
	$CLU_POW  = $BIG_POW;
	$CLU_ENE  = $BIG_ENE;
}

$EB_CLU     = (($CLU_POW           ) * $LATENCY) / $K;
$EB_CLU_RAM = (($CLU_POW + $RAM_POW) * $LATENCY) / $K;

$TB_CLU = $LATENCY / $K;

if ($VEC eq "inter") {
	$EB_CLU     = $EB_CLU     / 16;
	$EB_CLU_RAM = $EB_CLU_RAM / 16;
	$TB_CLU     = $TB_CLU     / 16;
}


$EB_CLU     *= 1e3;
$EB_CLU_RAM *= 1e3;
$TB_CLU     *= 1e3;

if ($N == 128) {
	print ",,,,,,,,,,,,,,,,,,,,\n";
}

print "$PROC,$TYPE,$GEN,$VEC,$N,$K,$R,$SNR,$FRA,$LD_ST_DEC,$CTHR,$ITHR,$LATENCY,$CLU_FREQ,$CLU_POW,$CLU_ENE,$RAM_POW,$RAM_ENE,$EB_CLU,$EB_CLU_RAM,$TB_CLU\n";
