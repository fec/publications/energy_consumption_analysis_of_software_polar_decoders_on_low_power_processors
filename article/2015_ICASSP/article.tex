% Template for ICASSP-2016 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,graphicx}
\usepackage{color}
\usepackage{pgfplots}
\usepackage{xspace}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{siunitx}
\usepackage{bm}
\usepackage{cite}
\def\TODO#1{\colorbox{yellow}{\bf TODO: #1}}
\def\bl{big.\textsc{LITTLE}\xspace}
\def\big{big\xspace}
\def\little{\textsc{LITTLE}\xspace}
\def\odr{\textsc{Odroid}\xspace}
\def\odrx{\textsc{Odroid-XU+E}\xspace}
\def\juno{\textsc{Juno}\xspace}
\def\mus{\si{\micro\second}}

\newcommand{\Arikan}{Ar{\i}kan }
\newcommand{\MISSREF}[1]{\textcolor{red}{\textbf{[MISS REF:#1]}}}

\title{Energy Consumption Analysis of Software Successive Cancellation
Polar Decoders on Low Power Processors}
%
% Single address.
% ---------------
\name{Adrien Cassagne$^{\star \dagger}$
\quad Bertrand Le Gal$^{\star}$
\quad Camille Leroux$^{\star}$
\quad Olivier Aumage$^{\dagger}$
\quad Denis Barthou$^{\dagger}$}

\address{$^{\star}$ IMS Lab, Bordeaux INP, France\\
$^{\dagger}$ Inria /  Labri, Univ. Bordeaux, INP, France}

\begin{document}
\maketitle
\begin{abstract}
% The abstract should appear at the top of the left-hand column of text, about
% 0.5 inch (12 mm) below the title area and no more than 3.125 inches (80 mm) in
% length.  Leave a 0.5 inch (12 mm) space between the end of the abstract and the
% beginning of the main text.  The abstract should contain about 100 to 150
% words, and should be identical to the abstract text submitted electronically
% along with the paper cover sheet.  All manuscripts must be in English, printed
% in black ink.
This paper presents a study on software Successive Cancellation polar
decoders on ARM architectures. It explores optimization schemes,
hardware and applicative parameters and their impacts on performance
and energy efficiency.
%\TODO{Abstract}
\end{abstract}
%
\begin{keywords}
Error Correction Codes, Polar Codes, Successive Cancellation decoding, 
Energy Consumption, Low Power Processors
%, ARM32, ARM64, SIMDization
%One, two, three, four, five
%\TODO{Keywords}
\end{keywords}
%
\section{Introduction}
\label{sec:intro}

Channel coding for error correction enables transmitting data over unreliable 
communication channels. While error correction coding/decoding is usually 
performed by dedicated hardware circuits on communication devices, the evolution 
of low power general purpose processors in terms of energy efficiency and 
parallelism (vector processing, number of cores,...) drives a growing interest 
for software ECC implementations (e.g. LDPC
decoders~\cite{Wan13a,legal2015a,Gal:2015aa}, \emph{Turbo}
decoders~\cite{6167744,6810402}). Recently, a 
new family of error correction codes has been introduced : polar codes. These 
codes asymptotically reach the capacity of various communication channels 
\cite{arikan2009}. Polar codes can be decoded using a successive 
cancellation (SC) decoder which has extensively been implemented in
hardware~\cite{Raymond:2014aa,mishra_successive_2012,Leroux:2012aa,Li:2014aa,Yuan:2014aa,sarkis_fast_polar_2014,Giard:2015aa}.
Alternatively, some software decoders implementations were proposed 
\cite{sarkis2014_1, sarkis2014_2, le-gal2014, le-gal2015, cassagne2015}, 
all relying on Single Instruction Multiple Data 
(SIMD) instruction set and reaching performance ranging upto multi-Gb/s. 
%This kind of instruction set is 
%available on most of today’s processor cores. 
%It resulted in several hundreds 
%of Mb/s to multi-Gb/s software decoders. 
Two schemes show high performance for software decoders: Intra-frame
parallelism \cite{sarkis2014_1, sarkis2014_2} provides a high
throughput and low latency solution. Inter-frame parallelism
\cite{le-gal2014, le-gal2015} improves the use of the SIMD unit and
the throughput, at the expense of a higher latency. The P-EDGE
framework \cite{cassagne2015} is the first software SC decoder that
includes both optimization schemes, with other state-of-the-art
optimizations. 

The optimization space exploration for SC
decoding of Polar codes has primarily been conducted with raw
performance in mind, while the energy consumption minimization should
also be factored in. Moreover, heterogeneous multicore processors such
as ARM's \bl architectures offer cores with widely different
performance and energy consumption profiles, further increasing the
number of design and run-time options to an overwhelming amount, and
in an non-trivial pattern. In this context, the contribution of this
paper is to leverage our P-EDGE framework to derive key guidelines and
general strategies in balancing performance and energy consumption
characteristics of software SC decoders.

%\TODO{This paper is organized as follows\dots}

\section{Polar codes encoding and decoding}

\subsection{Systematic Encoding}
Polar codes are linear block codes of size $N=2^n$, $n$ being a natural number. 
In \cite{arikan2009}, \Arikan defined their construction based on 
the $n^{th}$ Kronecker power of a kernel matrix 
$\kappa = \left[\begin{array}{cc}1& 0\\ 1& 1\end{array}\right] $. 
${\kappa}^{\otimes n}$ denotes the $n^{th}$ Kronecker power of $\kappa$.
The systematic encoding process \cite{arikan2011} consists in building an 
$N$-bits vector $V$ that includes $K$ information bits and $N-K$ frozen bits, 
usually set to zero. The location of the frozen bits depends on both the type of 
channel that is considered and the noise power on the channel 
\cite{arikan2009}. Then, a first encoding phase is performed: 
$U=V*\kappa^{\otimes n}$ and bits of $U$ in the frozen location are replaced by 
zeros. The codeword is finally obtained with a second encoding phase: 
$X=U*\kappa^{\otimes n}$. In this systematic form $X$ includes $K$ information 
bits and $N-K$ redundancy bits located on the frozen locations.

\subsection{SC Decoding}

%\begin{figure}[t]
%\centering
%\includegraphics[width=0.5\linewidth]{./diagrams/3_nodes}
%\caption{Node update data flow graph.\TODO{modif figure pour coller avec le 
%  texte, Mettre des noeuds carrés ?}}
%\label{fig:node}
%\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=1.0\linewidth]{./diagrams/tree_representation_SC_decoder}
\caption{Full SC decoding tree ($N = 16$)}
\label{fig:dec_tree}
\end{figure}

After being sent over the transmission channel, the noisy version of the 
codeword $X$ is received in the form of a log likelihood ratio (LLR) vector $Y$. 
The successive cancellation decoder successively estimates each bit $u_i$ 
based on the vector $Y$ and the previously estimated bits 
($\hat{u}_{0:i-1}$)\footnote{$\hat{u}_{0:i-1}=[\hat{u}_0 ... \hat{u}_{i-1}]$}. 
In order to estimate each bit $u_i$, the decoder computes the following LLR 
value:
{\small
\begin{equation*}
\lambda_{i}^{0} = \log\frac{\Pr(Y,\hat{u}_{0:i-1}|u_i=0)}{\Pr(Y,\hat{u}_{0:i-1}|u_i=1)}.
\end{equation*}
}
The estimated bit $\hat{u}_i$ is calculated based on the rule that is expressed 
as $\hat{u}_i=0$ if $\lambda_{i}^{0}>0$, $1$ otherwise. 
Since the decoder knows the location of the frozen bits, if $u_i$ is a frozen 
bit, $\hat{u}_i=0$ regardless of $\lambda_{i}^{0}$ value.
The SC decoding process can be seen as the traversal of a binary tree as shown 
in Figure~\ref{fig:dec_tree}. The tree includes $\log N + 1$ layers each 
including $2^d$ nodes, where $d$ is the depth of the layer in the tree. Each 
node contains a set of $2^{n-d}$ LLRs and partial sums $\hat{s}$. Nodes are 
visited using a pre-order traversal. As shown in Figure~\ref{fig:dec_tree}, 
three different functions are used for node updates. The $f$ function is applied 
when a left child node is accessed: 
$\lambda_{i}^{left}=f(\lambda_{i}^{up},\lambda_{i+2^d}^{up}),0\leq i<2^d$. 
The $g$ function is used when a right child node is accessed: 
$\lambda_{i}^{right}=g(\lambda_i^{up},\lambda_{i+2^d}^{up}),0\leq i<2^d$ finally 
when moving up in the tree from a right child node, the first half of partial 
sum is updated with $s_i^{up}=h(s_i^{left}, s_i^{right}),0\leq i<2^d/2$ 
and the second half is left unchanged. 
%updated with $h_2$: 
%$s_{i+2^l}^{up}=h_2(s_i^{right}),0\leq i<2^d/2$.
{\small
\begin{eqnarray*}
\left\{\begin{array}{l c l}
f(\lambda_a,\lambda_b) &=& sign(\lambda_a.\lambda_b).\min(|\lambda_a|,|\lambda_b|)\\
g(\lambda_a,\lambda_b,s)&=&(1-2s)\lambda_a+\lambda_b\\
h(s_a,s_b)&=& (s_{a} \oplus s_{b}, s_{b})
%h_2(s_a)&=& s_a.
\end{array}\right.
\label{eq:f_g}
\end{eqnarray*}
}
%{\small
%\begin{eqnarray*}
%\left\{\begin{array}{l c l c l}
%\lambda_c &=& f(\lambda_a,\lambda_b) &=& sign(\lambda_a.\lambda_b).\min(|\lambda_a|,|\lambda_b|)\\
%\lambda_c &=& g(\lambda_a,\lambda_b,s)&=&(1-2s)\lambda_a+\lambda_b\\
%(s_{c}, s_{d}) &=& h(s_{a}, s_{b}) &=& (s_{a} \oplus s_{b}, s_{b}).
%\end{array}\right.
%\label{eq:f_g}
%\end{eqnarray*}
%}
The decoding process stops when the partial sums of the root node is updated. In
a systematic polar encoding scheme, the partial sums of the root node is the 
decoded codeword.

%\subsection{Simplified SC decoding}

The computation tree can be pruned when taking into account the location of the 
frozen bits \cite{alamdar-yazdi2011,sarkis2014_1} and
%Polar codes can actually be 
%seen as a multi-level coding scheme in which a code is built based on smaller 
%sub-codes. In other words, 
some of the subtrees in the full binary tree can be 
replaced by specialized nodes. This 
tree pruning strategy %reduces the number of computation and 
creates extra 
parallelism to exploit in the lower levels of the tree.
%This is especially efficient since the 
%lower in the tree, the lower the parallelism.

\section{Software SC Decoders State-of-The-Art}

In \cite{sarkis2014_1, sarkis2014_2}, SIMD units are used to process several 
LLRs in parallel during the decoding of a frame. We denote this as 
\emph{intra-frame} vectorization. This approach is efficient in the upper layers 
of the tree and in the modified nodes with sufficient amount of parallelism. 
Intra-frame vectorization is however penalized when reaching the lowest layers 
of the tree where the computation becomes more sequential.\\ 
In \cite{le-gal2014, le-gal2015}, an alternative scheme called 
\emph{inter-frame} vectorization decodes several independent frames 
in parallel in order to saturate the SIMD unit. This 
approach improves the throughput of the SC decoder but it requires to load 
several frames before starting to decode, increasing
decoding latency and increasing the  
memory footprint of the decoder.\\
P-EDGE, a complete software SC decoding framework
was proposed in \cite{cassagne2015}. This multiplatform tool (x86-SSE, x86-AVX, 
ARM32-NEON, ARM64-NEON) includes all state-of-the-art advances in software SC 
decoding of Polar codes: It supports sequential/intra/inter-frame vectorization, 
different data format (\texttt{char}, \texttt{int}, \texttt{float}) and all 
existing tree pruning strategies. It includes some code generation strategies in 
order to trade flexibility (code rate $R$, code length $N$) for extra 
performance. The code generated is single-threaded.\\
All of these implementations aim at improving the throughput and/or latency. 
However in an embedded system environment, the energy consumption is a crucial
parameter. In this study, we propose to investigate the influence of several 
parameters on the energy consumption of SC software polar decoders on embedded 
processors.

\section{Exploring Parameters}
\label{sec:study}
%\TODO{Consommation "reelle" d'un soft vs la consommation}
The objective and originality of this study is to explore different software 
and hardware parameters for the execution of a software SC decoders on 
modern ARM architectures. For a software decoder such as P-EDGE, many 
parameters can be explored, influencing performance and energy efficiency. 
The target rate and frame size are applicative parameters, the kind of
SIMDization of the code (intra-frame or inter-frame for instance) and
the different optimizations are decoder parameters and the target
architecture, its frequency (and voltage) are hardware parameters. We
show in this study the correlations between these parameters, in order
to better choose the right implementation for a given applicative
framework. We chose for the testbed, detailed thereafter, an ARM32 and
ARM64 platforms based on big.LITTLE architecture, representative of
modern multi-core and heterogenous architectures. The SC decoder is
P-EDGE~\cite{cassagne2015}, enabling the comparison of different
vectorization schemes.

The flexibility of the P-EDGE framework enables to modify many
parameters and turn many optimizations on or off, leading to a large
amount of potential combinations. For the purpose of this study,
computations are performed with 8-bit fixed-point datatypes and all tree
pruning optimizations are activated. The main metrics we consider is the 
average amount of energy in Joules to decode one bit of information, expressed 
as  $E_b = (P \times l) / (K \times n_f)$ where $P$ is the average power 
(Watts), $l$ is the latency (\mus), $K$ the number of information bits and 
$n_f$ is the number of frames decoded in parallel (in the inter-frame 
implementation $n_f > 1$).

%amount of potential combinations. For the purpose of this exploratory study,
%a 8-bit fixed point format was selected because it provides near-floating point 
%decoding performance and fully benefits from the SIMD parallelism. The tree 
%pruning strategy does not affect decoding performance and reduce the number of 
%computation.\\ 
%The main metrics we consider is the average amount of energy in Joules to decode 
%one bit of information, expressed as  $E_b = (P \times l) / (K \times n_f)$ 
%where $P$ is the average power (Watts), $l$ is the latency (\mus), $K$ the 
%number of information bits and $n_f$ is the number of frames decoded in parallel 
%(in the inter-frame implementation $n_f > 1$).
%The latency $l$ is the cumulate time of the data loading into the decoder, the
%decoder processing time and the data storage time after the decoding stage.
%>>>>>>> 6f4206e7d4cef7eb7f3a89dd929d862562b0c573

\textbf{Testbed.} We conducted experiments on two low power
ARM~\bl platforms, an \odrx board using a 32-bit Samsung Exynos 5410
CPU and the reference 64-bit \juno Development Platform from ARM running
a Linux operating system. The two boards are detailed in Table~\ref{tab:specs}.

\begin{table}[h]
\begin{center}
{\footnotesize
\begin{tabular}{c | c | c}
                                    & \textbf{ODROID-XU+E} &          \textbf{\juno} \\
  \hline  
  \multirow{2}{*}{\textbf{SoC}}     &  Samsung Exynos 5410 &               ARM64 \bl \\
                                    &      (Exynos 5 Octa) &         (dev. platform) \\
  \hline  
  \multirow{1}{*}{\textbf{Arch.}}   &       32-bit, ARMv7  &           64-bit, ARMv8 \\
  \hline  
  \multirow{1}{*}{\textbf{Process}} &                 28nm &  unspecified (32/28 nm) \\

  \hline  
  \multirow{4}{*}{\textbf{\big}}    &  4xCortex-A15 MPCore &     2xCortex-A57 MPCore \\
                                    &   freq. [0.8-1.6GHz] &     freq. [0.45-1.1GHz] \\
                                    &   L1I 32KB, L1D 32KB &      L1I 48KB, L1D 32KB \\
                                    &               L2 2MB &                  L2 2MB \\
  \hline  
  \multirow{4}{*}{\textbf{\little}} &   4xCortex-A7 MPCore &     4xCortex-A53 MPCore \\
                                    &   freq. [250-600MHz] &      freq. [450-850MHz] \\
                                    &   L1I 32KB, L1D 32KB &      L1I 32KB, L1D 32KB \\
                                    &             L2 512KB &                  L2 1MB \\
\end{tabular}
}
\end{center}
\caption{Specifications of the \odr and the \juno boards.}
\label{tab:specs}
\end{table}


The \big and the \little clusters of cores on the \odr board are
on/off in a mutually exclusive way. The active cluster is selected
through Linux \texttt{cpufreq} mechanism: the \little cluster has a frequency range 
of 250-600~MHz, while the \big
cluster has a range of 800-1600~MHz. Both clusters can be activated together
or separately on the \juno board.
%and the frequency of each cluster can be set independently.
The \odr platform is able to report details about supply voltage, current
amperage and power consumption for each cluster and for
the RAM. The \juno platform reports voltage, current
amperage, power consumption and cumulated consumed energy for each
cluster, but no detail is available about the RAM.
Consequently, most experiments have been primarily conducted on the \odr
platform to benefit from the additional insight provided by the RAM
metrics.

\section{Experiments and Measurements}
\label{sec:measurements}

\begin{table}[h]
\begin{center}
{\footnotesize
\begin{tabular}{c | c | c | c | c | c}
  \textbf{Cluster} & 
  \textbf{Impl.} &  
  $\boldsymbol{T_i}$ \textbf{(Mb/s)} & 
  $\boldsymbol{l}$ \textbf{($\boldsymbol{\mu}$s)} &
  $\boldsymbol{E_b}$ \textbf{(nJ)} & 
  $\boldsymbol{P}$ \textbf{(W)}\\
  \hline  
  \multirow{3}{*}{\textbf{A7-450MHz}}  & seq.  &  3.1 &  655 &  37.8 & 0.117 \\
                                       & intra & 16.2 &  126 &   7.6 & 0.124 \\
                                       & inter & 21.8 & 1506 &   6.0 & 0.131 \\
  \hline  
  \multirow{3}{*}{\textbf{A53-450MHz}} & seq.  &  2.6 &  802 &  25.4 & 0.065 \\
                                       & intra & 13.5 &  152 &   5.4 & 0.073 \\
                                       & inter & 18.3 & 1790 &   4.9 & 0.091 \\
  \hline  
  \multirow{3}{*}{\textbf{A15-1.1GHz}} & seq.  &  7.5 &  274 & 122.0 & 0.913 \\
                                       & intra & 43.4 &   47 &  22.6 & 0.977 \\
                                       & inter & 67.9 &  482 &  16.4 & 1.114 \\
  \hline  
  \multirow{3}{*}{\textbf{A57-1.1GHz}} & seq.  & 10.3 &  198 &  75.0 & 0.775 \\
                                       & intra & 46.2 &   44 &  17.2 & 0.793 \\
                                       & inter & 66.4 &  493 &  13.8 & 0.918 \\
\end{tabular}
}
\end{center}
\caption{Main decoding characteristics for each cluster at given frequencies 
  ($T_i$ is the information throughput and $l$ is the latency for 
  single-threaded decoders).
  Fixed frame size $N = 4096$ and rate $R = 1/2$. 
  Only generated decoders are shown. The RAM consumption is not reported in the 
  \emph{energy-per-bit} ($E_b$) and in the \emph{power} ($P$).}
\label{tab:res}
\end{table}

Table~\ref{tab:res} gives an overview of the decoder behaviour on
different clusters and for various implementations. The sequential
version is mentioned for reference only as the information throughput
$T_i$ is much higher on vectorized versions, and generally the
inter-frame SIMD strategy deliver better performance at the cost of a
higher latency $l$. Table~\ref{tab:res} also compares the energy
consumption of \little and \big clusters. The A53 consumes less energy
than the A7 and the A57 consumes less energy than the A15. This can be
explained by some architectural improvements brought by the ARM64.
Despite the facts that the ARM64 we used is a development board, while
our ARM32 board uses a production processor commonly found in
smartphones, it outperforms the ARM32 architecture. Finally we observe
that the power $P$ vary with the decoder implementation. The SIMD units
consume more than the scalar pipeline. The power consumption is higher
for the inter-frame version than for the intra-frame one because it
fills the SIMD units more intensively.

\begin{figure}[htp]
\centering
\includegraphics[width=0.5\textwidth]{graphics/outputs/A15_1100MHz_R05_Eb_intra_inter_modif}
\caption{Variation of the \emph{energy-per-bit} ($E_b$) depending on the frame 
  size (various impl.: intra-, inter-frame, code gen. on/off). 
  Running on 32-bit ARM Cortex-A15 @ 1.1GHz. Fixed rate $R = 1/2$.
  Dark colors and light colors stand for CPU cluster and RAM energy 
  consumption, resp.}
\label{plot:frame_size}
\end{figure}

Figure~\ref{plot:frame_size} shows the \emph{energy-per-bit} consumption 
depending on the frame size $N$ for the fixed rate $R = 1/2$.
In general, the energy consumption increases with the frame size.
For small frame sizes ($N = 256$ and $N = 4096$), the inter-frame SIMD
outperforms the intra-frame SIMD.
This is especially true for $N = 256$ which has a low ratio of
SIMD computations over scalar computations in the intra-frame
version. As the frame size increases, the ratio of SIMD vs scalar
computations increases as well.
At some point around $N = 65536$ the intra-frame
implementation begins to outperform the inter-frame one because the data
for the intra-frame decoder still fits in the CPU cache whereas the data of
the inter-frame decoder does not fit the cache anymore. In our case (8-bit fixed point numbers
and 128-bit vector registers) the inter-frame decoders require 16 times
more memory than the intra-frame decoders. Then, for the frame size $N =
1048576$, both intra and inter-frame decoders now exceed the cache capacity
and the RAM power consumption becomes more significant due to the
increased number of cache misses causing RAM transactions. In general the code
generation is effective on the intra-frame strategy whereas it is
negligible on the inter-frame version of the code.

Considering those previous observations, it is more energy efficient to use 
inter-frame strategy for small frame sizes, whereas it is better to apply 
intra-frame strategy for higher frame sizes (comparable energy consumption with 
much lower latency).

\begin{figure}[htp]
\centering
\includegraphics[width=0.5\textwidth]{graphics/outputs/A7_250MHz_550MHz_A15_800MHz_1100MHz_R05_N4046_intra_inter_modif}
\caption{Variation of the \emph{energy-per-bit} ($E_b$) depending on the cluster
  frequency (various impl.: intra-, inter-frame, code gen. on). 
  Running on 32-bit ARM Cortex-A7 on the left and on ARM Cortex-A15 on 
  the right, resp. Fixed frame size and code rate $R = 1/2$.}
\label{plot:freq}
\end{figure}

Figure~\ref{plot:freq} shows the impact of the frequency on the energy.
We fixed the frame size $N$ and the code rate $R$. On both A7 and A15
clusters, the supply voltage increases with the frequency from 0.946V to
1.170V. The Cortex-A7 \little cluster results show that the energy
consumed by the system RAM is significant: At 250MHz it accounts for
half of the energy cost. Indeed, at low frequency, the long execution
time due to the low throughput causes a high dynamic RAM refreshing
bill. It is therefore more interesting to use frequencies higher than
250MHz. For this problem size and configuration, and from an energy-only
point of view, the best choice is to run the decoder at 350MHz. On the
Cortex-A15 \big cluster, the energy cost is mainly driven by the CPU
frequency, while the RAM energy bill is limited compared to the CPU. 

Thus, the bottom line about energy vs frequency relationship is: On the
\little cluster it is more interesting to clock the CPU at high
frequency (higher throughput and smaller latency for an additional
little energy cost); On the \big cluster, when the RAM consumption is
less visible, it is better to clock the CPU at a low frequency.

\begin{figure}[htp]
\centering
\includegraphics[width=0.45\textwidth]{graphics/outputs/rate_N32768_SNR25}
\caption{Variation of the \emph{energy-per-bit} ($E_b$) depending on the rate 
  $R = K / N$ (various impl.: intra-, inter-frame, code gen. on). 
  Running on 32-bit ARM Cortex-A7 @ 450MHz and 64-bit ARM Cortex-A53 and 
  A57 @ 450MHz. Fixed frame size.}
\label{plot:rate}
\end{figure}

In Figure~\ref{plot:rate} the \emph{energy-per-bit} cost decreases when
the code rate increases. This is expected because there are many more
information bits in the frame when $R$ is high, making the decoder more
energy efficient. With high rates, the SC decoding tree can be pruned
more effectively, making the decoding process even more energy
efficient. Figure~\ref{plot:rate} also compares the ARM Cortex-A7, A53
and A57 clusters for the same 450MHz frequency (note: this frequency is
not available on the A15). The \little Cortex-A7 is more energy
efficient than the \big Cortex-A57, and the \little A53 is itself more
energy efficient than the \little A7 ($E_{b_{A53}} < E_{b_{A7}} < E_{b_{A57}}$).

%\begin{figure}[htp]
%\centering
%\includegraphics[width=0.5\textwidth]{graphics/outputs/A7_A53_450MHz_A15_A57_1100MHz_R05_N4046_intra_inter_modif}
%\caption{Comparision of the \emph{energy-per-bit} ($E_b$) variation between the 
%  different ARM architectures. The LITTLE clusters (32-bit A7 in blue and 64-bit 
%  A53 in red) are compared on the left whereas the big clusters (32-bit A15 in 
%  green and 64-bit A57 in brown) are compared on the right. 
%  Fixed frame size $N = 4096$ and rate $R = 1/2$.}
%\label{plot:cluster}
%\end{figure}

\section{Conclusion and Future Work}
\label{sec:conc}
This paper presents for the first time a study comparing performance and energy
consumption for software Successive Cancellation polar decoders on
big.LITTLE ARM32 and ARM64 processors. The software used, P-EDGE,
integrates several state-of-the-art optimizations for such decoder, in
particular inter-frame and intra-frame vectorization. The results
highlight the fact that SIMDization improves energy efficiency, even
though the SIMD pipeline consumes more power than the scalar
pipeline. Moreover, we have shown that the execution of the decoder on
a processor running at low frequency is less efficient than when it
runs at a higher frequency, the reason being that the resulting memory energy
consumption increase cancels any gain obtained on the CPU side.  Comparing
ARM32 and ARM64 architectures, while the ARM64 architecture considered
is an experimental platform, it outperforms the older ARM32 processor
in terms of energy efficiency and performance.

Finally, for the software optimization, intra-frame vectorization
clearly outperforms inter-frame vectorization for large frames (lower
latency, comparable energy efficiency) while the reverse is true for
smaller frames, the intra-frame vectorization then suffering from the
lack of vectorization opportunities in the computation.


%\section{Acknowledgements}
%This study has been carried out with financial support from the French 
%State, managed by the French National Research Agency (ANR) in the frame 
%of the "Investments for the future" Programme IdEx Bordeaux - CPU 
%(ANR-10-IDEX-03-02).

%\section{COPYRIGHT FORMS}
%\label{sec:copyright}

% You must submit your fully completed, signed IEEE electronic copyright release
% form when you submit your paper. We {\bf must} have this form before your paper
% can be published in the proceedings.

%\TODO{Copyright Form}



% -------------------------------------------------------------------------
%\vfill\pagebreak

%\section{REFERENCES}
%\label{sec:refs}

%List and number all bibliographical references at the end of the
%paper. The references can be numbered in alphabetic order or in
%order of appearance in the document. When referring to them in
%the text, type the corresponding reference number in square
%brackets as shown at the end of this sentence \cite{C2}. An
%additional final page (the fifth page, in most cases) is
%allowed, but must contain only references to the prior
%literature.

% References should be produced using the bibtex program from suitable
% BiBTeX files (here: strings, refs, manuals). The IEEEbib.bst bibliography
% style file from IEEE produces unsorted bibliography list.
% -------------------------------------------------------------------------
\bibliographystyle{IEEEbib}
\bibliography{article}

\end{document}
% These guidelines include complete descriptions of the fonts, spacing, and
% related information for producing your proceedings manuscripts. Please follow
% them and if you have any questions, direct them to Conference Management
% Services, Inc.: Phone +1-979-846-6800 or email
% to \\\texttt{papers@icassp2016.org}.
% 
% \section{Formatting your paper}
% \label{sec:format}
% 
% All printed material, including text, illustrations, and charts, must be kept
% within a print area of 7 inches (178 mm) wide by 9 inches (229 mm) high. Do
% not write or print anything outside the print area. The top margin must be 1
% inch (25 mm), except for the title page, and the left margin must be 0.75 inch
% (19 mm).  All {\it text} must be in a two-column format. Columns are to be 3.39
% inches (86 mm) wide, with a 0.24 inch (6 mm) space between them. Text must be
% fully justified.
% 
% \section{PAGE TITLE SECTION}
% \label{sec:pagestyle}
% 
% The paper title (on the first page) should begin 1.38 inches (35 mm) from the
% top edge of the page, centered, completely capitalized, and in Times 14-point,
% boldface type.  The authors' name(s) and affiliation(s) appear below the title
% in capital and lower case letters.  Papers with multiple authors and
% affiliations may require two or more lines for this information. Please note
% that papers should not be submitted blind; include the authors' names on the
% PDF.
% 
% \section{TYPE-STYLE AND FONTS}
% \label{sec:typestyle}
% 
% To achieve the best rendering both in printed proceedings and electronic proceedings, we
% strongly encourage you to use Times-Roman font.  In addition, this will give
% the proceedings a more uniform look.  Use a font that is no smaller than nine
% point type throughout the paper, including figure captions.
% 
% In nine point type font, capital letters are 2 mm high.  {\bf If you use the
% smallest point size, there should be no more than 3.2 lines/cm (8 lines/inch)
% vertically.}  This is a minimum spacing; 2.75 lines/cm (7 lines/inch) will make
% the paper much more readable.  Larger type sizes require correspondingly larger
% vertical spacing.  Please do not double-space your paper.  TrueType or
% Postscript Type 1 fonts are preferred.
% 
% The first paragraph in each section should not be indented, but all the
% following paragraphs within the section should be indented as these paragraphs
% demonstrate.
% 
% \section{MAJOR HEADINGS}
% \label{sec:majhead}
% 
% Major headings, for example, "1. Introduction", should appear in all capital
% letters, bold face if possible, centered in the column, with one blank line
% before, and one blank line after. Use a period (".") after the heading number,
% not a colon.
% 
% \subsection{Subheadings}
% \label{ssec:subhead}
% 
% Subheadings should appear in lower case (initial word capitalized) in
% boldface.  They should start at the left margin on a separate line.
%  
% \subsubsection{Sub-subheadings}
% \label{sssec:subsubhead}
% 
% Sub-subheadings, as in this paragraph, are discouraged. However, if you
% must use them, they should appear in lower case (initial word
% capitalized) and start at the left margin on a separate line, with paragraph
% text beginning on the following line.  They should be in italics.
% 
% \section{PRINTING YOUR PAPER}
% \label{sec:print}
% 
% Print your properly formatted text on high-quality, 8.5 x 11-inch white printer
% paper. A4 paper is also acceptable, but please leave the extra 0.5 inch (12 mm)
% empty at the BOTTOM of the page and follow the top and left margins as
% specified.  If the last page of your paper is only partially filled, arrange
% the columns so that they are evenly balanced if possible, rather than having
% one long column.
% 
% In LaTeX, to start a new column (but not a new page) and help balance the
% last-page column lengths, you can use the command ``$\backslash$pagebreak'' as
% demonstrated on this page (see the LaTeX source below).
% 
% \section{PAGE NUMBERING}
% \label{sec:page}
% 
% Please do {\bf not} paginate your paper.  Page numbers, session numbers, and
% conference identification will be inserted when the paper is included in the
% proceedings.
% 
% \section{ILLUSTRATIONS, GRAPHS, AND PHOTOGRAPHS}
% \label{sec:illust}
% 
% Illustrations must appear within the designated margins.  They may span the two
% columns.  If possible, position illustrations at the top of columns, rather
% than in the middle or at the bottom.  Caption and number every illustration.
% All halftone illustrations must be clear black and white prints.  Colors may be
% used, but they should be selected so as to be readable when printed on a
% black-only printer.
% 
% Since there are many ways, often incompatible, of including images (e.g., with
% experimental results) in a LaTeX document, below is an example of how to do
% this \cite{Lamp86}.
% 
% \section{FOOTNOTES}
% \label{sec:foot}
% 
% Use footnotes sparingly (or not at all!) and place them at the bottom of the
% column on the page on which they are referenced. Use Times 9-point type,
% single-spaced. To help your readers, avoid using footnotes altogether and
% include necessary peripheral observations in the text (within parentheses, if
% you prefer, as in this sentence).
% 
% Below is an example of how to insert images. Delete the ``\vspace'' line,
% uncomment the preceding line ``\centerline...'' and replace ``imageX.ps''
% with a suitable PostScript file name.
% -------------------------------------------------------------------------
%\begin{figure}[htb]
%
%\begin{minipage}[b]{1.0\linewidth}
%  \centering
%  \centerline{\includegraphics[width=8.5cm]{image1}}
%%  \vspace{2.0cm}
%  \centerline{(a) Result 1}\medskip
%\end{minipage}
%%
%\begin{minipage}[b]{.48\linewidth}
%  \centering
%  \centerline{\includegraphics[width=4.0cm]{image3}}
%%  \vspace{1.5cm}
%  \centerline{(b) Results 3}\medskip
%\end{minipage}
%\hfill
%\begin{minipage}[b]{0.48\linewidth}
%  \centering
%  \centerline{\includegraphics[width=4.0cm]{image4}}
%%  \vspace{1.5cm}
%  \centerline{(c) Result 4}\medskip
%\end{minipage}
%%
%\caption{Example of placing a figure with experimental results.}
%\label{fig:res}
%%
%\end{figure}


% To start a new column (but not a new page) and help balance the last-page
% column length use \vfill\pagebreak.
% -------------------------------------------------------------------------
%\vfill
%\pagebreak

