set macro

# set text font and size
set terminal pdf enhanced font 'Helvetica,14' size 5,3
set encoding utf8

# start define plot styles ####################################################
# Palette URL:
# http://colorschemedesigner.com/#3K40zsOsOK-K-

red_000    = "#F9B7B0"
red_025    = "#F97A6D"
red_050    = "#E62B17"
red_075    = "#8F463F"
red_100    = "#6D0D03"

blue_000   = "#A9BDE6"
blue_025   = "#7297E6"
blue_050   = "#1D4599"
blue_075   = "#2F3F60"
blue_100   = "#031A49"

green_000  = "#A6EBB5"
green_025  = "#67EB84"
green_050  = "#11AD34"
green_075  = "#2F6C3D"
green_100  = "#025214"

brown_000  = "#F9E0B0"
brown_025  = "#F9C96D"
brown_050  = "#E69F17"
brown_075  = "#8F743F"
brown_100  = "#6D4903"

grid_color = "#6a6a6a"
text_color = "#6a6a6a"

my_line_width = "3.75"
my_axis_width = "1.2"
my_ps = "0.75"

set pointsize @my_ps

# set the style for the set 1, 2, 3...
set style line 1 linetype 5  linecolor rgbcolor  blue_025 linewidth @my_line_width
set style line 2 linetype 4  linecolor rgbcolor  blue_050 linewidth @my_line_width
set style line 3 linetype 7  linecolor rgbcolor   red_025 linewidth @my_line_width
set style line 4 linetype 6  linecolor rgbcolor   red_050 linewidth @my_line_width
set style line 5 linetype 9  linecolor rgbcolor green_025 linewidth @my_line_width
set style line 6 linetype 8  linecolor rgbcolor green_050 linewidth @my_line_width
set style line 7 linetype 11 linecolor rgbcolor brown_025 linewidth @my_line_width
set style line 8 linetype 10 linecolor rgbcolor brown_050 linewidth @my_line_width

# this is to use the user-defined styles we just defined.
set style increment user

# set the color and width of the axis border
set border 31 lw @my_axis_width lc rgb text_color

# set key options
set key top right width -2 height 0 font 'Helvetica, 10'

# set grid color
set grid lc rgb grid_color

# end define plot styles ######################################################

set xlabel "Rate (R = K / N)"
set ylabel "Energy-per-bit (nJ)"

#set logscale x 2
set xtics border in scale 0,0 nomirror rotate by -45  offset character 0, 0, 0 autojustify
set xtics ("1/10" 1, "2/10" 2, "3/10" 3, "4/10" 4, "5/10" 5, "6/10" 6, "7/10" 7, "8/10" 8, "9/10" 9)

###############################################################################

set xrange [1:9]
set title "Energy consumption depending on the frame rate (N = 2048)"
datafile = "./data/rate_N2048_SNR25.dat"
set output "./outputs/rate_N2048_SNR25.pdf"
plot datafile using 1:20 i 0 with linespoint title columnheader(1),\
     datafile using 1:20 i 1 with linespoint title columnheader(1),\
     datafile using 1:20 i 2 with linespoint title columnheader(1),\
     datafile using 1:20 i 3 with linespoint title columnheader(1),\
     datafile using 1:20 i 4 with linespoint title columnheader(1),\
     datafile using 1:20 i 5 with linespoint title columnheader(1)


set xrange [1:9]
set title "Energy consumption depending on the frame rate (N = 32768)"
datafile = "./data/rate_N32768_SNR25.dat"
set output "./outputs/rate_N32768_SNR25.pdf"
plot datafile using 1:20 i 0 with linespoint title columnheader(1),\
     datafile using 1:20 i 1 with linespoint title columnheader(1),\
     datafile using 1:20 i 2 with linespoint title columnheader(1),\
     datafile using 1:20 i 3 with linespoint title columnheader(1),\
     datafile using 1:20 i 4 with linespoint title columnheader(1),\
     datafile using 1:20 i 5 with linespoint title columnheader(1)

###############################################################################
